/*
 * adc_system.h
 *
 * Created: 1/19/2013 13:54:39
 *  Author: mattiku
 */ 


#ifndef ADC_SYSTEM_H_
#define ADC_SYSTEM_H_

void adc_system_init();
unsigned char adc_get_value();

#endif /* ADC_SYSTEM_H_ */