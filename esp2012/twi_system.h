/*
 * twi_system.h
 *
 * Created: 1/16/2013 20:04:19
 *  Author: mattiku
 */ 

#ifndef TWI_SYSTEM_H_
#define TWI_SYSTEM_H_

// todo: rename to TWI_
#define NO_ERRORS 0
#define ERROR_START_FAILURE 1
#define ERROR_SLA_W_FAILURE 2
#define ERROR_SLA_R_FAILURE 3
#define ERROR_DATA_FAILURE 4

int twi_get_error_status();
void twi_system_init();
void twi_uninit();
void twi_send_start();
void twi_send_rep_start();
void twi_send_stop();
void twi_send_sla_w(unsigned char sla);
void twi_send_sla_r(unsigned char sla);
void twi_send_data_8bit(unsigned char data);
void twi_read_data_8bit_nack(unsigned char* data);
void twi_read_data_8bit_ack(unsigned char* data);

#endif /* TWI_SYSTEM_H_ */