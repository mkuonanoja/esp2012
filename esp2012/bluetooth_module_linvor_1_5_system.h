/*
 * bluetooth_module_linvor_1_5_system.h
 *
 * Created: 1/20/2013 16:22:27
 *  Author: mattiku
 */ 

#ifndef BLUETOOTH_MODULE_LINVOR_1_5_SYSTEM_H_
#define BLUETOOTH_MODULE_LINVOR_1_5_SYSTEM_H_

void bluetooth_module_linvor_1_5_system_init(long baud_rate);
void bluetooth_module_linvor_1_5_set_device_name(char* name);
void bluetooth_module_linvor_1_5_get_device_name(char* name, int max_length);
void bluetooth_module_linvor_1_5_get_device_version(char* version, int max_size);
void bluetooth_module_linvor_1_5_set_device_baud_rate(long baud_rate);
void bluetooth_module_linvor_1_5_set_parity(int parity);
void bluetooth_module_linvor_1_5_set_device_pin_code(char* pin_code);

#endif /* BLUETOOTH_MODULE_LINVOR_1_5_SYSTEM_H_ */