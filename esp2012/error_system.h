/*
 * error_system.h
 *
 * Created: 1/14/2013 00:16:27
 *  Author: mattiku
 */ 

#ifndef ERROR_SYSTEM_H_
#define ERROR_SYSTEM_H_

void error_system_init();

// Set error status led off
// Clears error log from EEPROM
void error_reset_log();

// Set error status led on
// Stores error message to EEPROM
void error_report(char* error_message);

// sends the whole log
void error_send_log_to_bluetooth();

#endif /* ERROR_SYSTEM_H_ */