/*
 * error_system.c
 *
 * Created: 1/14/2013 00:17:23
 *  Author: mattiku
 */ 
#define F_CPU 8000000UL

#define SLOT_MESSAGE_BLOCK_SIZE	50 
#define SLOT_MESSAGE_BLOCK_COUNT 5

#include "error_system.h"
#include "led_system.h"
#include "bluetooth_system.h"
#include "storage_system.h"
#include "clock_system.h"
#include <util/delay.h>
#include <stdio.h>

static char error_reported = 0;

void blink_error_led();

void error_system_init()
{
    clock_regiter_task(blink_error_led, 100);
}

void error_reset_log()
{
	error_reported = 0;
	for (int i = 0; i < SLOT_MESSAGE_BLOCK_COUNT; i++) {
		storage_store_log_message("");
	}
}

void error_report(char* error_message)
{
    if (error_reported == 0)
	storage_store_log_message(error_message);
	error_reported = 1;
}

void error_send_log_to_bluetooth()
{
	char temp[50];
	char buffer[SLOT_MESSAGE_BLOCK_SIZE];

    bluetooth_send_text("ERROR LOG (EEPROM)\r\n");
	for (int i = 0; i < SLOT_MESSAGE_BLOCK_COUNT; i++) {
		sprintf(temp, "SLOT %i: [", i);
		bluetooth_send_text(temp);

		storage_load_log_message(i, buffer, SLOT_MESSAGE_BLOCK_SIZE);
		buffer[SLOT_MESSAGE_BLOCK_SIZE-1] = '\0';
		bluetooth_send_text(buffer);
		
		bluetooth_send_text("]\r\n");
	}
}

void blink_error_led()
{
    static int led_state = 0;
    led_state++;
    if (led_state % 2 == 0 && error_reported) {
        led_set(LED_STATUS_ERROR, LED_STATE_ON);
    } else {
        led_set(LED_STATUS_ERROR, LED_STATE_OFF);
    }    
}
