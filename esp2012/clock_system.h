/*
 * clock_system.h
 *
 * Created: 1/21/2013 00:06:29
 *  Author: mattiku
 */ 


#ifndef CLOCK_SYSTEM_H_
#define CLOCK_SYSTEM_H_

void clock_system_init();
long clock_get_system_time_ms();

// @param callback is the callback function which does the task
// @param interval_ms is the delay between task calls
void clock_regiter_task(void (*callback)(), long interval_ms );

#endif /* CLOCK_SYSTEM_H_ */