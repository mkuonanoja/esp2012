/*
 * Created: 1/12/2013 22:48:52
 *  Author: mattiku
 */ 

#ifndef BLUETOOTH_SYSTEM_H_
#define BLUETOOTH_SYSTEM_H_

void bluetooth_system_init();

void bluetooth_send_text(char* text);

// @argument text pointer to text buffer
// @param max_length the max size of the text written to the buffer
// @return length of the text
int bluetooth_get_next_command(char* text, int max_length);

void bluetooth_get_device_version(char* version, int max_length);
void bluetooth_get_device_name(char* name, int max_length);
void bluetooth_reset_command_buffer();

#endif /* BLUETOOTH_SYSTEM_H_ */