/*
 * Created: 1/12/2013 22:48:52
 *  Author: mattiku
 */ 


#ifndef SENSOR_SYSTEM_H_
#define SENSOR_SYSTEM_H_

void sensor_system_init();

// @return 0-255
unsigned char sensor_get_pressure();

// @return acceleration x-vector (G)
double sensor_get_acceleration_x();

// @return acceleration y-vector (G)
double sensor_get_acceleration_y();

// @return acceleration z-vector (G)
double sensor_get_acceleration_z();

#endif /* SENSOR_SYSTEM_H_ */