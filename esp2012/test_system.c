/*
 * Created: 1/12/2013 22:53:08
 *  Author: mattiku
 */ 
#define F_CPU 8000000UL 

#include "test_system.h"
#include "led_system.h"
#include "button_system.h"
#include "audio_system.h"
#include "sensor_system.h"
#include <util/delay.h>

void test_main_loop()
{

	#ifdef ACCELERATION_SENSOR_TEST_ENABLED
	while (1) {
		_delay_ms(10);
		if (sensor_get_acceleration_z() > 0) {
			led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
		} else {
			led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
		}			
	}
	#endif // ACCELERATION_SENSOR_TEST_ENABLED

	#ifdef PRESSURE_SENSOR_TEST_ENABLED
	while (1) {
		_delay_ms(10);
		if (sensor_get_pressure() < 50) {
            led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
            led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
		} else {
			led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
		}			
	}
	#endif
      

	#ifdef AUDIO_PLAYBACK_TEST_ENABLED
	audio_play_sound(SOUND_2);
	while (1) {
		_delay_ms(10);
	}
	#endif

	#ifdef LED_TEST_ENABLED
 
	int led_on = 0;
	while (1) {
		led_on = (led_on + 1) % 2;
		if (led_on) {
			led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_ON, LED_STATE_ON);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_ON, LED_STATE_ON);
			led_set(LED_STATUS_POWER, LED_STATE_ON);
			led_set(LED_STATUS_BLUETOOTH, LED_STATE_ON);
			led_set(LED_STATUS_ERROR, LED_STATE_ON);
            led_set(LED_DEVICE_MODE, LED_STATE_ON);
		}
		else {
			led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
			led_set(LED_STATUS_POWER, LED_STATE_OFF);
			led_set(LED_STATUS_BLUETOOTH, LED_STATE_OFF);
			led_set(LED_STATUS_ERROR, LED_STATE_OFF);
            led_set(LED_DEVICE_MODE, LED_STATE_OFF);
		}
		_delay_ms(1000);
	}
	#endif // LED_TEST_ENABLED
          led_set(LED_STATUS_ERROR, LED_STATE_ON);   

	#ifdef BUTTON_A_TEST_ENABLED
	led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
	led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
	while (1) {
		if (button_is_button_A_pressed() || button_is_button_B_pressed()) {
            button_reset_button_A();
            button_reset_button_B();
			led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
			_delay_ms(1000);
			led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
			led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
		}
	}
	#endif

	#ifdef MODE_BUTTON_TEST_ENABLED
	led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
	led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
    int mode = 0;
	while (1) {
        if (button_is_button_B_pressed()) {
            button_reset_button_B();
            mode = (mode + 1) % 2;
            if (mode)
                led_set(LED_DEVICE_MODE, LED_STATE_ON);
            else
                led_set(LED_DEVICE_MODE, LED_STATE_OFF);
        }
	}
	#endif
    
    #ifdef JTAG_TEST_ENABLED
	while (1) {
        led_set(LED_STATUS_POWER, LED_STATE_OFF);
       	_delay_ms(50);
        led_set(LED_STATUS_POWER, LED_STATE_ON);
        _delay_ms(50);
	}    
    #endif // JTAG_TEST_ENABLED
}
