/*
 * Created: 1/12/2013 22:48:52
 *  Author: mattiku
 */ 


#ifndef BUTTON_SYSTEM_H_
#define BUTTON_SYSTEM_H_

#define MODE_SWITCH_STATE_PLAY 1
#define MODE_SWITCH_STATE_CONTROL 2

#define BUTTON_STATE_PRESSED = 1
#define BUTTON_STATE_NOT_PRESSED = 2

// Button System API

void buttom_system_init();

// @return true 1 if button A is pressed otherwise return 0
// @note You must call reset_button_A() to reset the button A state.
unsigned char button_is_button_A_pressed();
void button_reset_button_A();

unsigned char button_is_button_B_pressed();
void button_reset_button_B();



#endif /* BUTTON_SYSTEM_H_ */