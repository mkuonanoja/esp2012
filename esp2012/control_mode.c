/*
 * control_mode.c
 *
 * Created: 1/16/2013 16:56:33
 *  Author: mattiku
 */ 

#include "control_mode.h"
#include "led_system.h"
#include "bluetooth_system.h"
#include "sensor_system.h"
#include "storage_system.h"
#include "button_system.h"
#include "error_system.h"
#include "audio_system.h"
#define F_CPU 8000000UL 
#include <string.h>
#include <stdlib.h>
#include <stdio.h>
#include <util/delay.h>

#define COMMAND_BUFFER_SIZE 50
#define RESPONSE_BUFFER_SIZE 100

void send_rgb_led_statuses_to_bluetooth();
void send_pressure_status_to_bluetooth();
void send_acceleration_sensor_status_to_bluetooth();
void play_sound(char* command);
void set_rgb_leds(char* command);
void toggle_device_mode();
void send_button_A_state_to_bluetooth();
void send_bluetooth_module_info_to_bluetooth();
void send_uptime_info_to_bluetooth();
void send_help_to_bluetooth();

static char last_command[COMMAND_BUFFER_SIZE] = "";

void handle_commands()
{
	char command_buffer[COMMAND_BUFFER_SIZE];
	int command_size = bluetooth_get_next_command(command_buffer, COMMAND_BUFFER_SIZE);
    if (command_size == -1)
        return;

    if (command_size == 0 && strlen(last_command) > 0) {
        strcpy(command_buffer, last_command);
        command_size = strlen(command_buffer);
    }
    if (command_size > 0) {
		if( (command_size >= 3) && (strncmp(command_buffer, "led", 3) == 0)) {
            // "led"
            if (command_size == 3) {
                strcpy(last_command, command_buffer);
			    send_rgb_led_statuses_to_bluetooth();
            }
            // "led[a,b][1,0][1,0][1,0]"
            if (command_size >= 7) {
                strcpy(last_command, command_buffer);
                set_rgb_leds(command_buffer);
            }
		}
        // "nose"
		if( (command_size >= 4) && (strncmp(command_buffer, "nose", 4) == 0)) {
            strcpy(last_command, command_buffer);
			send_pressure_status_to_bluetooth();
		}
        // "acc"
		if( (command_size >= 3) && (strncmp(command_buffer, "acc", 3) == 0)) {
            strcpy(last_command, command_buffer);
            send_acceleration_sensor_status_to_bluetooth();
		}
        // "play[1,2,3]"
		if( (command_size >= 4) && (strncmp(command_buffer, "play", 4) == 0)) {
            strcpy(last_command, command_buffer);
            play_sound(command_buffer);
		}
        // "button"
        if( (command_size >= 5) && (strncmp(command_buffer, "button", 5) == 0)) {
            strcpy(last_command, command_buffer);
            send_button_A_state_to_bluetooth();
        }
        // "mode"
        if( (command_size >= 4) && (strncmp(command_buffer, "mode", 4) == 0)) {
            strcpy(last_command, command_buffer);
            toggle_device_mode();
        }
        // "errors"
        if( (command_size >= 6) && (strncmp(command_buffer, "errors", 6) == 0)) {
            strcpy(last_command, command_buffer);
            error_send_log_to_bluetooth();
        }
        // "bt"
        if( (command_size >= 2) && (strncmp(command_buffer, "bt", 2) == 0)) {
            strcpy(last_command, command_buffer);
            send_bluetooth_module_info_to_bluetooth();
        }
        // "uptime"
        if( (command_size >= 6) && (strncmp(command_buffer, "uptime", 6) == 0)) {
            strcpy(last_command, command_buffer);
            send_uptime_info_to_bluetooth();
        }
        // "help"
        if( (command_size >= 4) && (strncmp(command_buffer, "help", 4) == 0)) {
            strcpy(last_command, command_buffer);
            send_help_to_bluetooth();
        }
	}
}

void toggle_device_mode()
{
    // todo: get_device_m
    if (storage_load_device_mode() == DEVICE_MODE_PLAY) {
        storage_store_device_mode(DEVICE_MODE_CONTROL);
    } else {
        storage_store_device_mode(DEVICE_MODE_PLAY);        
    }
}

void send_button_A_state_to_bluetooth()
{
    if (button_is_button_A_pressed()) {
        bluetooth_send_text("Button A has been pressed.\r\n");
    } else {
        bluetooth_send_text("Button A hasn't been pressed.\r\n");
    }
    button_reset_button_A();
}

void set_rgb_leds(char* command)
{
    int led_id = 0;
    int red_led_state = LED_STATE_OFF;
    int green_led_state = LED_STATE_OFF;
    int blue_led_state = LED_STATE_OFF;

    if (command[3] == 'A' || command[3] == 'a')
        led_id = LED_RGB_LEFT;
    if (command[3] == 'B' || command[3] == 'b')
        led_id = LED_RGB_RIGHT;

    if (command[4] == '1')
        red_led_state = LED_STATE_ON;

    if (command[5] == '1')
        green_led_state = LED_STATE_ON;

    if (command[6] == '1')
        blue_led_state = LED_STATE_ON;

    led_set_rgb(led_id, red_led_state, green_led_state, blue_led_state);
}

void play_sound(char* command)
{
    int sound_id = atoi(command+4);
    if (sound_id >= 1 && sound_id <=3) {
        audio_play_sound(sound_id);
    }
}

void send_rgb_led_statuses_to_bluetooth()
{
	char response_buffer[RESPONSE_BUFFER_SIZE];
	unsigned char red, green, blue;		
	char red_state_text[5];
	char green_state_text[5];
	char blue_state_text[5];

	// Left eye led
	led_get_led_rgb(LED_RGB_LEFT, &red, &green, &blue);
	if (red == LED_STATE_ON)
		strcpy(red_state_text, "ON");
	else
		strcpy(red_state_text, "OFF");
	if (green == LED_STATE_ON)
		strcpy(green_state_text, "ON");
	else
		strcpy(green_state_text, "OFF");
	if (blue == LED_STATE_ON)
		strcpy(blue_state_text, "ON");
	else
		strcpy(blue_state_text, "OFF");
	sprintf(response_buffer,"Color left eye:  %s,%s,%s\r\n", red_state_text, green_state_text, blue_state_text);
	bluetooth_send_text(response_buffer);

	// Right eye led
	led_get_led_rgb(LED_RGB_RIGHT, &red, &green, &blue);
	if (red == LED_STATE_ON)
		strcpy(red_state_text, "ON");
	else
		strcpy(red_state_text, "OFF");
	if (green == LED_STATE_ON)
		strcpy(green_state_text, "ON");
	else
		strcpy(green_state_text, "OFF");
	if (blue == LED_STATE_ON)
		strcpy(blue_state_text, "ON");
	else
		strcpy(blue_state_text, "OFF");
	sprintf(response_buffer,"Color right eye: %s,%s,%s\r\n", red_state_text, green_state_text, blue_state_text);
	bluetooth_send_text(response_buffer);
}

void send_pressure_status_to_bluetooth()
{
	char response_buffer[RESPONSE_BUFFER_SIZE];
	int i = sensor_get_pressure();
	sprintf(response_buffer, "Pressure sensor status: %i\r\n", i);
	bluetooth_send_text(response_buffer);
}

void send_acceleration_sensor_status_to_bluetooth()
{
	char response_buffer[RESPONSE_BUFFER_SIZE];
    char x_text[10];
    char y_text[10];
    char z_text[10];
    double acceleration_x = sensor_get_acceleration_x();
    double acceleration_y = sensor_get_acceleration_y();
    double acceleration_z = sensor_get_acceleration_z();
    dtostrf(acceleration_x, 3, 2, x_text);
    dtostrf(acceleration_y, 3, 2, y_text);
    dtostrf(acceleration_z, 3, 2, z_text);
	sprintf(response_buffer, "Acceleration sensor status: %s, %s, %s\r\n", x_text, y_text, z_text);
	bluetooth_send_text(response_buffer);
}


void send_bluetooth_module_info_to_bluetooth()
{
    char response[30];
    char text[50];
    
    bluetooth_get_device_version(response, 20);
    sprintf(text, "Bluetooth device: %s\r\n", response);
    bluetooth_send_text(text); 
    
    bluetooth_get_device_name(response, 20);
    sprintf(text, "Bluetooth name: %s\r\n", response);
    bluetooth_send_text(text);
}

void send_uptime_info_to_bluetooth()
{
    char buffer[100];
    sprintf(buffer, "Device has been powered up %i minutes after previous flashing.\r\n", (int)(storage_load_system_uptime_ms()/60000));
    bluetooth_send_text(buffer);
}

void send_help_to_bluetooth()
{
    #define LINE_DELAY_MS 50
    bluetooth_send_text("\r\nCOMMANDS:\r\n");
    bluetooth_send_text("  help ..................... Prints this help text.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  led ...................... Shows RGB led statuses.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  led[a,b][1,0][1,0][1,0] .. set RGB led statuses.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  nose ..................... Shows nose sensor value.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  acc ...................... Shows accelerometer value.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  play[1,2,3] .............. Plays given sound.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  button ................... Shows button status.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  mode ..................... Toggles play mode on/off.\r\n");
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  errors ................... Shows error log.\r\n");            
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  bt ....................... Shows bluetooth module info.\r\n");                
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  uptime ................... Shows device total uptime in minutes.\r\n");                
    _delay_ms(LINE_DELAY_MS);
    bluetooth_send_text("  <enter> .................. Executes last command.\r\n");                
    _delay_ms(LINE_DELAY_MS);
}
