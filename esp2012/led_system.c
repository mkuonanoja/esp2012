/*
 * Created: 1/12/2013 22:53:08
 *  Author: mattiku
 */ 
#include "led_system.h"
#include <avr/io.h>

static char led_rgb_left_state_red = LED_STATE_OFF;
static char led_rgb_left_state_green = LED_STATE_OFF;
static char led_rgb_left_state_blue = LED_STATE_OFF;
static char led_rgb_right_state_red = LED_STATE_OFF;
static char led_rgb_right_state_green = LED_STATE_OFF;
static char led_rgb_right_state_blue = LED_STATE_OFF;

void led_system_init()
{
	//Set pins 35-40  to outputs
	DDRA = 0x3F; // 0011 1111
	
	//turns off RGB leds (set pins 35-40 LOW)
	PORTA &= ~0x3F; // 1100 0000
	

	//Set pins 1-3  to outputs
	DDRB |= 0x07; // 0000 0111
	
	//turns off leds (set pins 1-3 LOW)
	PORTB &= ~0x07; // 1111 1000


	//Set pins 19  to output
	DDRD |= 1 << PD5; // 0010 0000
	
	//turns off led (set pin 19 LOW)
	PORTD &= ~(1 << PD5); // 0010 0000}
}

void led_set(unsigned led, unsigned char state )
{
	switch(led) {
		case LED_STATUS_POWER:
			if (state == LED_STATE_ON)
				PORTB |= (1 << PB0); // turns pin 1 to HIGH
			else
				PORTB &= ~(1 << PB0); // turns pin 1 to LOW
		break;
		
		case LED_STATUS_BLUETOOTH:
			if (state == LED_STATE_ON)
				PORTB |= (1 << PB1); // turns pin 2 to HIGH
			else
				PORTB &= ~(1 << PB1); // turns pin 2 to LOW
				
		break;
		
		case LED_STATUS_ERROR:
			if (state == LED_STATE_ON)
				PORTB |= (1 << PB2); // turns pin 3 to HIGH
			else
				PORTB &= ~(1 << PB2); // turns pin 3 to LOW
		break;
		
		case LED_DEVICE_MODE:
			if (state == LED_STATE_ON)
				PORTD |= (1 << PD5); // turns pin 19 to HIGH
			else
				PORTD &= ~(1 << PD5); // turns pin 19 to LOW
		break;
	}
}

void led_set_rgb( unsigned char led, unsigned char red_state, unsigned char green_state, unsigned char blue_state )
{
	switch(led) {
		case LED_RGB_LEFT:
			led_rgb_left_state_red = red_state;
			led_rgb_left_state_green = green_state;
			led_rgb_left_state_blue = blue_state;
			if (red_state == LED_STATE_ON)
				PORTA |= (1 << PA0); // turns pin 40 to HIGH
			else
				PORTA &= ~(1 << PA0); // turns pin 40 to LOW

			if (green_state == LED_STATE_ON)
				PORTA |= (1 << PA1); // turns pin 39 to HIGH
			else
				PORTA &= ~(1 << PA1); // turns pin 39 to LOW

			if (blue_state == LED_STATE_ON)
				PORTA |= (1 << PA2); // turns pin 38 to HIGH
			else
				PORTA &= ~(1 << PA2); // turns pin 38 to LOW
		break;	
		case LED_RGB_RIGHT:
			led_rgb_right_state_red = red_state;
			led_rgb_right_state_green = green_state;
			led_rgb_right_state_blue = blue_state;
		
			if (red_state == LED_STATE_ON)
				PORTA |= (1 << PA3); // turns pin 37 to HIGH
			else
				PORTA &= ~(1 << PA3); // turns pin 37 to LOW

			if (green_state == LED_STATE_ON)
				PORTA |= (1 << PA4); // turns pin 36 to HIGH
			else
				PORTA &= ~(1 << PA4); // turns pin 36 to LOW

			if (blue_state == LED_STATE_ON)
				PORTA |= (1 << PA5); // turns pin 35 to HIGH
			else
				PORTA &= ~(1 << PA5); // turns pin 35 to LOW
			break;
	}
}

void led_get_led_rgb( unsigned char led_id, unsigned char *red_state, unsigned char *green_state, unsigned char *blue_state )
{
	switch(led_id) {
		case LED_RGB_LEFT:
			*red_state = led_rgb_left_state_red;
			*green_state = led_rgb_left_state_green;
			*blue_state = led_rgb_left_state_blue;
		break;
		case LED_RGB_RIGHT:
			*red_state = led_rgb_right_state_red;
			*green_state = led_rgb_right_state_green;
			*blue_state = led_rgb_right_state_blue;
		break;
	}	
}
