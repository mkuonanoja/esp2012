/*
 * adc_system.c
 *
 * Created: 1/19/2013 13:54:55
 *  Author: mattiku
 */ 
#include "adc_system.h"
#include <avr/io.h>
#include <avr/interrupt.h>

static volatile unsigned char adc_value = 0;

void adc_system_init()
{
    ADCSRA |= 1 << ADEN; // enable ADC
    ADMUX |= 7; // set pin 33 to ADC mode
//    ADCSRA |= 1 << ADPS2; // set prescaler to 16 (62.5 kHz @ 1MHz MCU)                     
    ADCSRA |= (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // set frequency prescaler to 128 (62.5 kHz @ 8MHz MCU)                     
    ADMUX |= (1 << ADLAR); // set ADCH is the 8bit result (data is written in ADCH only)
    ADCSRA |= (1 << ADIE); // enable ADC interrupt
    ADMUX |= (1 << REFS0); // set refrence voltage to Vdd with capasitor
    
    sei();
    
    ADCSRA |= 1 << ADSC; // start conversion
}

ISR(ADC_vect)
{
    adc_value = ADCH;
    ADCSRA |= 1 << ADSC; // restart conversion
}

unsigned char  adc_get_value()
{
    return adc_value;	
}
