/*
 * play_mode.h
 *
 * Created: 1/16/2013 14:31:52
 *  Author: mattiku
 */ 


#ifndef PLAY_MODE_H_
#define PLAY_MODE_H_

// Runs gesture recognizion if no effect is running
void recognice_gestures();

// Updates RGB led status for the led effects
void update_led_effects();

#endif /* PLAY_MODE_H_ */