/*
 * acceleration_sensor_adxl345_system.h
 *
 * Created: 1/19/2013 14:03:21
 *  Author: mattiku
 */ 


#ifndef ACCELERATION_SENSOR_ADXL345_SYSTEM_H_
#define ACCELERATION_SENSOR_ADXL345_SYSTEM_H_

#define ACCELERATION_SENSOR_ADXL345_RANGE_2_G 2
#define ACCELERATION_SENSOR_ADXL345_RANGE_4_G 4
#define ACCELERATION_SENSOR_ADXL345_RANGE_8_G 8
#define ACCELERATION_SENSOR_ADXL345_RANGE_16_G 16 

void acceleration_sensor_adxl345_system_init(int accleleration_sensor_range);
double acceleration_sensor_adxl345_get_x();
double acceleration_sensor_adxl345_get_y();
double acceleration_sensor_adxl345_get_z();
void acceleration_sensor_adxl345_refresh();

#endif /* ACCELERATION_SENSOR_ADXL345_SYSTEM_H_ */