/*
 * Created: 1/12/2013 22:53:08
 *  Author: mattiku
 */ 

#include "button_system.h"
#include "clock_system.h"
#include <avr/interrupt.h>
#include <avr/io.h>

#define BUTTON_MIN_DELAY_MS 100

static volatile unsigned char button_A_pressed = 0;
static volatile unsigned char button_B_pressed = 0;
static volatile unsigned long button_a_last_time_ms = 0;
static volatile unsigned long button_b_last_time_ms = 0;

void init_pin_16();
void init_pin_20();

void buttom_system_init()
{
	init_pin_16();
	init_pin_20();
    sei();
}

unsigned char button_is_button_A_pressed()
{
	return button_A_pressed;
}

void button_reset_button_A()
{
	button_A_pressed = 0;
}

unsigned char button_is_button_B_pressed()
{
    return button_B_pressed;
}

void button_reset_button_B()
{
    button_B_pressed = 0;
}

void init_pin_16()
{
	PORTD |= 1 << PD2; // enable pullup resistor for pin 16
	DDRD &= ~(1 << PD2); // Set pin 16 to input pin
	PCICR |= (1 << PCIE3); // enable PCINT 24..31 interrupts
	PCMSK3 |= (1 << PCINT26); // enable PCINT3026  
}

void init_pin_20()
{
	PORTD |= 1 << PD6; // enable pullup resistor for pin 20
	DDRD &= ~(1 << PD6); // Set pin 20 to input pin	
    PCICR |= (1 << PCIE3); // enable PCINT 24..31 interrupts
    PCMSK3 |= (1 << PCINT30); // enable PCINT30    
}

ISR(PCINT3_vect)
{
    if (PIND & (1 << PD2)) {
        if (clock_get_system_time_ms() - button_a_last_time_ms > BUTTON_MIN_DELAY_MS) {
            button_A_pressed = 1;
            button_a_last_time_ms = clock_get_system_time_ms();
        }        
    }

    if (PIND & (1 << PD6)) {
        if (clock_get_system_time_ms() - button_b_last_time_ms > BUTTON_MIN_DELAY_MS) {
            button_B_pressed = 1;
            button_b_last_time_ms = clock_get_system_time_ms();
        }            
    }
}
