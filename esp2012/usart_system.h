/*
 * usart_system.h
 *
 * Created: 1/20/2013 16:14:28
 *  Author: mattiku
 */ 

#ifndef USART_SYSTEM_H_
#define USART_SYSTEM_H_

// todo: define baud rates here

void usart_system_init(long baud_rate);

void usart_send_text(char* text);

// raw data
int usart_get_text(char* text, int max_length);

void usart_set_line_delimiter(char delimiter);

// @return -1 if no lines, other wise return line size
int usart_get_next_line(char* text, int max_size);

#endif /* USART_SYSTEM_H_ */