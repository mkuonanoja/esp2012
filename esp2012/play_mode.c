/*
 * play_mode.c
 *
 * Created: 1/16/2013 14:32:02
 *  Author: mattiku
 */ 

#include "play_mode.h"
#include "sensor_system.h"
#include "button_system.h"
#include "audio_system.h"
#include "led_system.h"
#include "clock_system.h"
#include <math.h>

#define PRESSURE_TRIGGER_LEVEL	30

#define EFFECT_FOR_SQUEEZE_NOSE_GESTURE 1
#define EFFECT_FOR_PUSH_FOREHEAD_GESTURE 2
#define EFFECT_FOR_ZERO_GRAVITY_GESTURE 3
#define EFFECT_FOR_UPSIDE_DOWN_GESTURE 4
#define EFFECT_FOR_FREE_FALL_GESTURE 5
#define EFFECT_NOTHING_HAPPEND_GESTURE 6

#define TIMEOUT_FOR_SQUEEZE_NOSE_GESTURE_MS	2000
#define TIMEOUT_FOR_PRESS_FOREHEAD_GESTURE_MS 2000
#define TIMEOUT_FOR_ZERO_GRAVITY_GESTURE_MS 2000
#define TIMEOUT_FOR_UPSIDE_DOWN_GESTURE_MS 2000
#define TIMEOUT_FOR_FREE_FALL_GESTURE_MS 2000
#define TIMEOUT_FOR_NOTHING_HAPPENS_GESTURE_MS 3000

//#define ZERO_GRAVITY__THRESHOLD_G 2.0
#define UPSIDE_DOWN_THRESHOLD_G -0.8
#define FREE_FALL_THRESHOLD_G 0.15
#define THROW_UPWARD_THRESHOLD_G 1.3
#define TOP_ALTITUDE_REACHED_THRESHOLD_G 1.1
#define UPWARD_MOTION_TIME_MIN_THRESHOLD_MS 200
#define FREE_FALL_MOTION_TIME_MIN_THRESHOLD_MS 400

#define FREE_FALL_GESTURE_STATE_NOT_STARTED 0
#define FREE_FALL_GESTURE_STATE_ZERO_ACCELERATION_DETECTED 1
#define FREE_FALL_GESTURE_STATE_MOTION_TIME_ACCEPTED 2
    
#define ZERO_GRAVITY_GESTURE_STATE_NOT_STARTED 0
#define ZERO_GRAVITY_GESTURE_STATE_THROW_UPWARD_DETECTED 1
#define ZERO_GRAVITY_GESTURE_STATE_TOP_ALTITUDE_DETECTED 2

#define EYE_BLINK_SPEED_MS 50

#define LED_EFFECT_NONE 0
#define LED_EFFECT_BLINK_BLUE_EYES 1
#define LED_EFFECT_BLINK_RED_EYES 2
#define LED_EFFECT_BLINK_GREEN_EYES 3
#define LED_EFFECT_BLINK_WHITE_EYES 4
#define LED_EFFECT_BLINK_RED_BLUE_EYES 5
#define LED_EFFECT_PING_PONG 6

void start_gesture_effect(int effect);
long get_time_from_last_gesture_ms();

int detect_squeeze_nose_gesture();
int detect_press_forehead_gesture();
int detect_free_fall_gesture();
int detect_upsidedown_gesture();
int detect_zero_gravity_gesture();

static long gesture_time_out_ms = 0;
static long gesture_start_time_ms = 0;
static long active_led_effect = LED_EFFECT_NONE;

static int zero_gravity_gesture_state = ZERO_GRAVITY_GESTURE_STATE_NOT_STARTED;
static int free_fall_gesture_state = FREE_FALL_GESTURE_STATE_NOT_STARTED;
static int throw_upward_start_time_ms = 0;
static int free_fall_start_time_ms = 0;
static int reset_upside_down_effect = 0;

void recognice_gestures()
{
    // Disable gestures when last effect is ongoing
	if (get_time_from_last_gesture_ms() < gesture_time_out_ms) {
        button_reset_button_A();
		return;
	}
	
    if (detect_squeeze_nose_gesture()) {
		start_gesture_effect(EFFECT_FOR_SQUEEZE_NOSE_GESTURE);
        return;
    }
	
    if (detect_press_forehead_gesture()) {
        start_gesture_effect(EFFECT_FOR_PUSH_FOREHEAD_GESTURE);
        return;
    }

    if (detect_upsidedown_gesture()) {
        start_gesture_effect(EFFECT_FOR_UPSIDE_DOWN_GESTURE);
        return;
    }             

    if (detect_free_fall_gesture()) {
        start_gesture_effect(EFFECT_FOR_FREE_FALL_GESTURE);        
        return;
    }
    
	if (detect_zero_gravity_gesture()) {
         start_gesture_effect(EFFECT_FOR_ZERO_GRAVITY_GESTURE);
         return;
    }
    
	if (detect_nothing_happens_gesture()) {
    	start_gesture_effect(EFFECT_NOTHING_HAPPEND_GESTURE);
    	return;
	}
}

#define NOTHING_HAPPENES_THRESHOLD_MS 60000 // 1 minute
static long __last_time_something_happened_ms = 0;

int detect_nothing_happens_gesture()
{
    if (clock_get_system_time_ms() - __last_time_something_happened_ms > NOTHING_HAPPENES_THRESHOLD_MS) {
        return 1;        
    }
    return 0;
}

// @return 1 if gesture is detected
int detect_press_forehead_gesture()
{
	if (button_is_button_A_pressed()) {
    	button_reset_button_A();
        return 1;
	} else {
        return 0;
    }
}

// @return 1 if gesture is detected
int detect_squeeze_nose_gesture() 
{
 	if (sensor_get_pressure() < PRESSURE_TRIGGER_LEVEL) {
         return 1;
 	} else {
         return 0;
    }
}

// @return 1 if gesture is detected
int detect_upsidedown_gesture()
{
 	if (sensor_get_acceleration_z() < UPSIDE_DOWN_THRESHOLD_G ) {
         if (reset_upside_down_effect) {
            reset_upside_down_effect = 0;
            return 1;
         }            
 	} else {
        reset_upside_down_effect = 1;
    }
    return 0;    
}

// @return 1 if gesture is detected
int detect_free_fall_gesture()
{
    long system_time_ms = clock_get_system_time_ms();
    
	// Free fall gesture
	double gravity_x = sensor_get_acceleration_x();
	double gravity_y = sensor_get_acceleration_y();
	double gravity_z = sensor_get_acceleration_z();
	double gravity_vector = sqrt(gravity_x * gravity_x + gravity_y * gravity_y + gravity_z * gravity_z);

	switch (free_fall_gesture_state) {
    	case FREE_FALL_GESTURE_STATE_NOT_STARTED:
    	    if (gravity_vector < FREE_FALL_THRESHOLD_G ) {
        	    free_fall_start_time_ms = system_time_ms;
        	    free_fall_gesture_state = FREE_FALL_GESTURE_STATE_ZERO_ACCELERATION_DETECTED;
    	    }
    	break;

    	case FREE_FALL_GESTURE_STATE_ZERO_ACCELERATION_DETECTED:
    	    if (gravity_vector < FREE_FALL_THRESHOLD_G) {
            	if (system_time_ms - free_fall_start_time_ms > FREE_FALL_MOTION_TIME_MIN_THRESHOLD_MS) {
                	free_fall_gesture_state = FREE_FALL_GESTURE_STATE_MOTION_TIME_ACCEPTED;
            	}
    	    } else {
        	    free_fall_gesture_state = FREE_FALL_GESTURE_STATE_NOT_STARTED;
    	    }
    	break;
	}
    if (free_fall_gesture_state == FREE_FALL_GESTURE_STATE_MOTION_TIME_ACCEPTED) {
        free_fall_gesture_state = FREE_FALL_GESTURE_STATE_NOT_STARTED;
        return 1;
    } else {
        return 0;
    }
}

// triggers when:
// 1) enough acceleration is detected (force and time) (throwing phase)
// 2) normal acceleration is detected (top point)
//
// @return 1 if gesture is detected
int detect_zero_gravity_gesture()
{
    long system_time_ms = clock_get_system_time_ms();
    double gravity_x = sensor_get_acceleration_x();
    double gravity_y = sensor_get_acceleration_y();
    double gravity_z = sensor_get_acceleration_z();
    double gravity_vector = sqrt(gravity_x * gravity_x + gravity_y * gravity_y + gravity_z * gravity_z);
    int time_from_throw_start_ms = system_time_ms - throw_upward_start_time_ms;

    switch (zero_gravity_gesture_state) {
        case ZERO_GRAVITY_GESTURE_STATE_NOT_STARTED:
            if (gravity_vector > THROW_UPWARD_THRESHOLD_G) {
                throw_upward_start_time_ms = system_time_ms;
                zero_gravity_gesture_state = ZERO_GRAVITY_GESTURE_STATE_THROW_UPWARD_DETECTED;
                break;
            }
        break;

        case ZERO_GRAVITY_GESTURE_STATE_THROW_UPWARD_DETECTED:
            if (time_from_throw_start_ms > UPWARD_MOTION_TIME_MIN_THRESHOLD_MS) {
                if (gravity_vector < TOP_ALTITUDE_REACHED_THRESHOLD_G) {
                    zero_gravity_gesture_state = ZERO_GRAVITY_GESTURE_STATE_TOP_ALTITUDE_DETECTED;
                    break;
                }
            } else {
                if (gravity_vector < TOP_ALTITUDE_REACHED_THRESHOLD_G) {
                    zero_gravity_gesture_state = ZERO_GRAVITY_GESTURE_STATE_NOT_STARTED; // not time enough for going up motion
                    break;
                }
            }
        break;
    }

    if (zero_gravity_gesture_state == ZERO_GRAVITY_GESTURE_STATE_TOP_ALTITUDE_DETECTED) {
        zero_gravity_gesture_state = ZERO_GRAVITY_GESTURE_STATE_NOT_STARTED; 
        return 1;
    } else {
        return 0;
    }
}

void update_led_effects()
{
    long system_time_ms = clock_get_system_time_ms();
    
	long time_from_last_gesture_ms = get_time_from_last_gesture_ms(system_time_ms);
	if (time_from_last_gesture_ms > gesture_time_out_ms) {
		active_led_effect = LED_EFFECT_NONE;
	}
	if (active_led_effect == LED_EFFECT_NONE) {
		led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF); // the default play mode eye color
		led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF); // the default play mode eye color
		return;
	}
	
	char blink_on = (time_from_last_gesture_ms / EYE_BLINK_SPEED_MS / 2 ) % 2 == 0;
	
	switch (active_led_effect) {
		case LED_EFFECT_BLINK_BLUE_EYES:
			if (blink_on) {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON); 
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON); 
			} else {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF); 
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
			}
		break;
	
		case LED_EFFECT_BLINK_RED_EYES:
			if (blink_on) {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
			} else {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
			}
		break;

		case LED_EFFECT_BLINK_GREEN_EYES:
			if (blink_on) {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_ON, LED_STATE_OFF);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_ON, LED_STATE_OFF);
			} else {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF,LED_STATE_OFF);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF,LED_STATE_OFF);
			}
		break;
		
		case LED_EFFECT_BLINK_WHITE_EYES:
			if (blink_on) {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_ON, LED_STATE_ON);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_ON, LED_STATE_ON);
			} else {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
			}
		break;
		
		case LED_EFFECT_BLINK_RED_BLUE_EYES:
			if (blink_on) {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
			} else {
				led_set_rgb(LED_RGB_LEFT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
				led_set_rgb(LED_RGB_RIGHT, LED_STATE_ON, LED_STATE_OFF, LED_STATE_OFF);
			}
		break;
        
        case LED_EFFECT_PING_PONG:
			if (blink_on) {
    			led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_ON, LED_STATE_OFF);
    			led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
			} else {
    			led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_OFF);
    			led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_ON, LED_STATE_OFF);
			}
        break;
	}
}

long get_time_from_last_gesture_ms()
{
	return clock_get_system_time_ms() - gesture_start_time_ms;
}

void start_led_effect(int led_effect)
{
	active_led_effect = led_effect;	
}

void start_gesture_effect(int effect)
{
    long system_time_ms = clock_get_system_time_ms();
    __last_time_something_happened_ms = system_time_ms;
	switch(effect) {
		case EFFECT_FOR_SQUEEZE_NOSE_GESTURE:
 			gesture_start_time_ms = system_time_ms;
 			gesture_time_out_ms = TIMEOUT_FOR_SQUEEZE_NOSE_GESTURE_MS;
			start_led_effect(LED_EFFECT_BLINK_BLUE_EYES);
			audio_play_sound(SOUND_2);
		break;
		
		case EFFECT_FOR_PUSH_FOREHEAD_GESTURE:
 			gesture_start_time_ms = system_time_ms;
	 		gesture_time_out_ms = TIMEOUT_FOR_PRESS_FOREHEAD_GESTURE_MS;
			start_led_effect(LED_EFFECT_BLINK_RED_EYES);		
			audio_play_sound(SOUND_3);
		break;
		
		case EFFECT_FOR_ZERO_GRAVITY_GESTURE:
 			gesture_start_time_ms = system_time_ms;
			gesture_time_out_ms = TIMEOUT_FOR_ZERO_GRAVITY_GESTURE_MS;
			start_led_effect(LED_EFFECT_BLINK_GREEN_EYES);		
			audio_play_sound(SOUND_1);
		break;
		
		case EFFECT_FOR_UPSIDE_DOWN_GESTURE:
 			gesture_start_time_ms = system_time_ms;
			gesture_time_out_ms = TIMEOUT_FOR_UPSIDE_DOWN_GESTURE_MS;
			start_led_effect(LED_EFFECT_BLINK_WHITE_EYES);		
			audio_play_sound(SOUND_3);
		break;
		
		case EFFECT_FOR_FREE_FALL_GESTURE:
 			gesture_start_time_ms = system_time_ms;		
 			gesture_time_out_ms = TIMEOUT_FOR_FREE_FALL_GESTURE_MS;
    		start_led_effect(LED_EFFECT_BLINK_RED_BLUE_EYES);		
			audio_play_sound(SOUND_2);
		break;
        
        case EFFECT_NOTHING_HAPPEND_GESTURE:
 			gesture_start_time_ms = system_time_ms;
 			gesture_time_out_ms = TIMEOUT_FOR_NOTHING_HAPPENS_GESTURE_MS;
 			start_led_effect(LED_EFFECT_PING_PONG);
            audio_play_sound(SOUND_3);
        break;
	}
}
