/*
 * bluetooth_module_linvor_1_5_system.c
 *
 * Created: 1/20/2013 16:22:36
 *  Author: mattiku
 */ 
#define F_CPU 8000000UL

#include "bluetooth_module_linvor_1_5_system.h"
#include "usart_system.h"
#include "error_system.h"
#include <util/delay.h>
#include <stdio.h>
#include <string.h>

#define RESPONSE_BUFFER_SIZE 20
#define MAX_COMMAND_SIZE 20
#define AT_COMMAND_WAIT_TIME_MS 1400
#define MODULE_WAKE_UP_TIME_MS 600

#define PARITY_NONE 0
#define PARITY_EVEN 1
#define PARITY_ODD 2
#define MAX_DEVICE_VERSION_SIZE 20
#define MAX_DEVICE_NAME_SIZE 32

static char __device_version[MAX_DEVICE_VERSION_SIZE] = "";
static char __device_name[MAX_DEVICE_NAME_SIZE] = "";

void send_at_command_to_bluetooth_module(char* command, char* response, int max_response);
int test_at_commands();
void fetch_device_version();

void bluetooth_module_linvor_1_5_system_init(long baud_rate)
{
    _delay_ms(MODULE_WAKE_UP_TIME_MS); 
    if (!test_at_commands()) {
        error_report("Cannot found bluetooth module.");
        return;
    }
    fetch_device_version();
    if (strcmp(__device_version, "linvorV1.5") != 0) {
        error_report("Unknow bluetooth module.");
        error_report(__device_version);
        return;
    }
//     bluetooth_module_linvor_1_5_set_device_baud_rate(baud_rate);
//     bluetooth_module_linvor_1_5_set_parity(PARITY_NONE);
}

void send_at_command_to_bluetooth_module(char* command, char* response, int max_response)
{
    usart_send_text(command);
    _delay_ms(AT_COMMAND_WAIT_TIME_MS); // device handles given commands after a 1 second
    int response_length = usart_get_text(response, max_response);
    if (response_length == max_response)
        response_length = max_response - 1;
    response[response_length] = '\0';
}

// @return 1 if AT test is success, otherwise return 0
int test_at_commands()
{
    char response_buffer[RESPONSE_BUFFER_SIZE];
    send_at_command_to_bluetooth_module("AT", response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK"
    if (strncmp(response_buffer, "OK", 2) == 0) {
        return 1;
    } else {
        return 0;
    }
}

void bluetooth_module_linvor_1_5_set_device_name(char* name)
{
    char command_buffer[RESPONSE_BUFFER_SIZE];
    char response_buffer[RESPONSE_BUFFER_SIZE];
    sprintf(command_buffer, "AT+NAME%s", name);
    send_at_command_to_bluetooth_module(command_buffer, response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK<name>"
    if (strncmp(response_buffer, "OK", 2) == 0) {
        strcpy(__device_name, name);
    } else {
        error_report("Cannot set bluetooth device name.");
    }
}

void fetch_device_version(char* version, int max_size)
{
    char command_buffer[RESPONSE_BUFFER_SIZE];
    char response_buffer[RESPONSE_BUFFER_SIZE];
    sprintf(command_buffer, "AT+VERSION");
    send_at_command_to_bluetooth_module(command_buffer, response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK<version>"
    int response_length = strlen(response_buffer);
    const int NAME_START_POS = 2;    
    int i;
    for (i = NAME_START_POS; i < response_length; i++ ) {
        if (response_buffer[i] == '\r' || response_buffer[i] == '\n')
            break;
        __device_version[i - NAME_START_POS] = response_buffer[i];
    }
    __device_version[i - NAME_START_POS] = '\0';
}

void bluetooth_module_linvor_1_5_set_device_baud_rate(long baud_rate)
{
    char command_buffer[RESPONSE_BUFFER_SIZE];
    char response_buffer[RESPONSE_BUFFER_SIZE];
    switch(baud_rate) {
        case 1200: sprintf(command_buffer, "AT+BAUD1"); break;
        case 2400: sprintf(command_buffer, "AT+BAUD2"); break;
        case 4800: sprintf(command_buffer, "AT+BAUD3"); break;
        case 9600: sprintf(command_buffer, "AT+BAUD4"); break;
        case 19200: sprintf(command_buffer, "AT+BAUD5"); break;
        case 38400: sprintf(command_buffer, "AT+BAUD6"); break;
        case 57600: sprintf(command_buffer, "AT+BAUD7"); break;
        case 115200: sprintf(command_buffer, "AT+BAUD8"); break;
    }
    send_at_command_to_bluetooth_module(command_buffer, response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK<baud>"
    if (strncmp(response_buffer, "OK", 2) != 0) {
        error_report("Cannot set bluetooth baud rate.");
    }
}

void bluetooth_module_linvor_1_5_set_parity(int parity) 
{
    char response_buffer[RESPONSE_BUFFER_SIZE];    
    switch (parity) {
    case PARITY_NONE:
        send_at_command_to_bluetooth_module("AT+PN", response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK"
        if (strncmp(response_buffer, "OK", 2) != 0) {
            error_report("Cannot set bluetooth parity check.");
        }
    break;
    case PARITY_EVEN:
        send_at_command_to_bluetooth_module("AT+PE", response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK"
        if (strncmp(response_buffer, "OK", 2) != 0) {
            error_report("Cannot set bluetooth parity check.");
        }
    break;
    case PARITY_ODD:
        send_at_command_to_bluetooth_module("AT+PO", response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK"
        if (strncmp(response_buffer, "OK", 2) != 0) {
            error_report("Cannot set bluetooth parity check.");
        }
    break;
    }
}

void bluetooth_module_linvor_1_5_set_device_pin_code(char* pin_code)
{
    char command_buffer[RESPONSE_BUFFER_SIZE];
    char response_buffer[RESPONSE_BUFFER_SIZE];
    sprintf(command_buffer, "AT+PIN%s", pin_code);
    //    sprintf(command_buffer, "AT+PWD%s\r\n", pin);
    send_at_command_to_bluetooth_module(command_buffer, response_buffer, RESPONSE_BUFFER_SIZE); // -> "OK<pin>"
    if (strncmp(response_buffer, "OK", 2) != 0) {
        error_report("Cannot set bluetooth pin code.");
    }
}

void bluetooth_module_linvor_1_5_get_device_version(char* version, int max_length)
{
    if (max_length > MAX_DEVICE_VERSION_SIZE)
        max_length = MAX_DEVICE_VERSION_SIZE;
    strncpy(version, __device_version, max_length);
}

void bluetooth_module_linvor_1_5_get_device_name(char* name, int max_length)
{
    if (max_length > MAX_DEVICE_VERSION_SIZE)
        max_length = MAX_DEVICE_VERSION_SIZE;
    strncpy(name, __device_name, max_length);
}

