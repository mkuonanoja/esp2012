/*
 * storage_system.c
 *
 * Created: 1/15/2013 19:48:45
 *  Author: mattiku
 */ 

#include "storage_system.h"
#include <avr/eeprom.h>
#include <string.h>

#define SLOT_MESSAGE_BLOCK_SIZE	50
#define SLOT_MESSAGE_BLOCK_COUNT 5

unsigned char EEMEM __device_mode = DEVICE_MODE_PLAY;
unsigned char EEMEM eeprom_next_log_message_slot = 0;
unsigned long EEMEM eeprom_system_uptime_ms = 0;
char EEMEM eeprom_log_message1[SLOT_MESSAGE_BLOCK_SIZE] = "";
char EEMEM eeprom_log_message2[SLOT_MESSAGE_BLOCK_SIZE] = "";
char EEMEM eeprom_log_message3[SLOT_MESSAGE_BLOCK_SIZE] = "";
char EEMEM eeprom_log_message4[SLOT_MESSAGE_BLOCK_SIZE] = "";
char EEMEM eeprom_log_message5[SLOT_MESSAGE_BLOCK_SIZE] = "";

void storage_system_init()
{
    // nothing to do    
}

void storage_store_device_mode(char mode)
{
	eeprom_update_byte(&__device_mode, mode);
}

char storage_load_device_mode()
{
	return eeprom_read_byte(&__device_mode);	
}

#include "led_system.h"

void storage_store_log_message(char* message)
{
	char next_log_message_slot = eeprom_read_byte(&eeprom_next_log_message_slot);
	if (next_log_message_slot < 0 || next_log_message_slot >=SLOT_MESSAGE_BLOCK_COUNT) {
		return;
	}
	int size = strlen(message) + 1;
	if (size > SLOT_MESSAGE_BLOCK_SIZE)
		size = SLOT_MESSAGE_BLOCK_SIZE;
	switch (next_log_message_slot) {
		case 0: eeprom_update_block((void *)message, (void *)eeprom_log_message1, size); break;
		case 1: eeprom_update_block((void *)message, (void *)eeprom_log_message2, size); break;
		case 2: eeprom_update_block((void *)message, (void *)eeprom_log_message3, size); break;
		case 3: eeprom_update_block((void *)message, (void *)eeprom_log_message4, size); break;
		case 4: eeprom_update_block((void *)message, (void *)eeprom_log_message5, size); break;
	}
	
	next_log_message_slot = (next_log_message_slot + 1) % SLOT_MESSAGE_BLOCK_COUNT;
	eeprom_update_byte(&eeprom_next_log_message_slot, next_log_message_slot);
}

void storage_load_log_message(int slot, char* message, int max_size)
{
	if (slot < 0 || slot >= SLOT_MESSAGE_BLOCK_COUNT) {
		strcpy(message, "");
		return;
	}
    int copy_size = max_size;
    if (copy_size > SLOT_MESSAGE_BLOCK_SIZE )
        copy_size = SLOT_MESSAGE_BLOCK_SIZE;
	switch(slot) {
		case 0:	eeprom_read_block((void *)message, (const void *)eeprom_log_message1, copy_size); break;
		case 1:	eeprom_read_block((void *)message, (const void *)eeprom_log_message2, copy_size); break;
		case 2:	eeprom_read_block((void *)message, (const void *)eeprom_log_message3, copy_size); break;
		case 3:	eeprom_read_block((void *)message, (const void *)eeprom_log_message4, copy_size); break;
		case 4:	eeprom_read_block((void *)message, (const void *)eeprom_log_message5, copy_size); break;								
	}
}

void storage_store_system_uptime_ms(unsigned long time_ms)
{
    eeprom_update_dword(&eeprom_system_uptime_ms, time_ms);
}

unsigned long storage_load_system_uptime_ms()
{
    return eeprom_read_dword(&eeprom_system_uptime_ms);
}
