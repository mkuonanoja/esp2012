/*
 * Created: 1/12/2013 22:53:08
 *  Author: mattiku
 */ 

// Timer 2A produces PWM for pin 21
// Timer 0A reads audio data from PGM system and updates PWM output value

// todo: optimize by using a cache (512Bytes?)

#include "audio_system.h"
#include "audio_data.h"
#include "error_system.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <avr/pgmspace.h>

#define AUDIO_GAIN 3
#define SAMPLE_RATE 8000

#define STATE_NOT_PLAYING_SOUND 1
#define STATE_PLAYING_SOUND_1 2
#define STATE_PLAYING_SOUND_2 3
#define STATE_PLAYING_SOUND_3 4

static unsigned char audio_system_initialized = 0;
static char audio_system_state = STATE_NOT_PLAYING_SOUND;
static long sample_counter = 0; // the next sample to play
static long sample_count = 0;

void start_pwm();
void stop_pwm();

void setup_timer0A_for_playback_routine();

void setup_timer0A_for_playback_routine()
{
	TCCR0B = 0;
	TCCR0B |= (1 << CS01); // Start the timer0, prescale 1/8 = 1Mhz @ 8 MHz
	OCR0A = 124; // 1 Mhz -> 8 kHz
	TCCR0A = 0; // init
	TCCR0A |= (1 << WGM01); // set mode: clear time compare (CTC)
	TCNT0 = 0; // reset counter
	TIMSK0 |= (1 << OCIE0A); // enable interrupt for timer0A
	sei();    
}

void audio_system_init()
{
    setup_timer0A_for_playback_routine();
    stop_pwm();
    audio_system_initialized = 1;
}

void audio_play_sound( unsigned char sound )
{
 	if (!audio_system_initialized) {
		error_report("Audio system not initialized.");
	 	return;
 	}
	if (audio_system_state != STATE_NOT_PLAYING_SOUND)
		audio_stop_sound();
	 
	sample_counter = 0;
	switch (sound) {
		case SOUND_1:
			sample_count = sound_1_sample_count;
			audio_system_state = STATE_PLAYING_SOUND_1;
		break;
		
		case SOUND_2:
			sample_count = sound_2_sample_count;
			audio_system_state = STATE_PLAYING_SOUND_2;
		break;
		
		case SOUND_3:
			sample_count = sound_3_sample_count;
			audio_system_state = STATE_PLAYING_SOUND_3;
		break;
	}
    start_pwm();
}

void audio_stop_sound()
{
    stop_pwm();
	audio_system_state = STATE_NOT_PLAYING_SOUND;	
	sample_count = 0;
	sample_counter = 0;
}

unsigned char get_next_sample()
{
	unsigned char sample = 0;

	switch(audio_system_state) {
		case STATE_PLAYING_SOUND_1:
			if (sample_counter < sample_count)
				sample = (unsigned char)pgm_read_byte(&sound_1_samples[sample_counter++]);
		break;
		
		case STATE_PLAYING_SOUND_2:
			if (sample_counter < sample_count)		
				sample = (unsigned char)pgm_read_byte(&sound_2_samples[sample_counter++]);
		break;
		
		case STATE_PLAYING_SOUND_3:
			if (sample_counter < sample_count)				
				sample = (unsigned char)pgm_read_byte(&sound_3_samples[sample_counter++]);
		break;
	}
	
	int s = (sample - 128) * AUDIO_GAIN + 128;
	if (s < 0)
		s = 0;
	if (s > 255)
		s = 255;
	sample = s;

	return sample;	
}

void read_next_sample()
{
	if (audio_system_state == STATE_NOT_PLAYING_SOUND )
	{
		OCR2A = 0; // set speaker out to LOW
		return;
	}
	if (sample_counter >= sample_count-16) {
    	audio_stop_sound();
	}
	OCR2A = get_next_sample();

}


void start_pwm()
{
    // Set timer 2A to produce PWM to PD7 pin
    DDRD |= (1 << 7); // Set PD7 to output
    PORTD &= ~(1 << 7); // Set output PD7 to 0

    //TCCR2B = 0; // init
    TCCR2B |= (1 << CS20); // Start the timer2, set clock source 1/1 = 8Mhz @ 8Mhz
    //	TCCR2B |= (1 << CS22) | (1 << CS20); // Start the timer2, set clock source 1/128 = 62.5Khz @ 8Mhz
    TCCR2A = 0; // init
    TCCR2A |= (1 << WGM21) | (1 << WGM20); // Set mode to Fast PWM, top = 0xFF
    TCCR2A |= (1 << COM2A1); // Clear OC2 on compare match, set OC2A at TOP
    //FOC2A = 0 ??
    OCR2A = 0; // reset comparation
    TCNT2 = 0; // reset counter
}

void stop_pwm()
{
    TCCR2B = 0; // stop timer 2
    PORTD &= ~(1 << 7); // set output for pin 21 to LOW
}

// Reads samples and control PWM output
ISR (TIMER0_COMPA_vect)
{
    unsigned char sreg =SREG;
    cli();
	read_next_sample();
    SREG = sreg;
}

