/*
 * clock_system.c
 *
 * Created: 1/21/2013 00:06:40
 *  Author: mattiku
 */ 

#include "clock_system.h"
#include "error_system.h"
#include <avr/io.h>
#include <avr/interrupt.h>

#define MAX_TASK_CALLBACKS 10
#define TICK_FREQUENCY 1000

static volatile long long __tics = 0;
static volatile long __system_time_ms = 0;
static int __callback_count = 0;
static long __callback_intervals_ms[MAX_TASK_CALLBACKS];
static volatile long __callback_time_from_last_call_ms[MAX_TASK_CALLBACKS];

static void (*__callback[MAX_TASK_CALLBACKS])();

void clock_system_init()
{
	cli();
    
    // Setup timer 1A to trigger interrupt with given TICK_FREQUENCY 
    TCCR1A = 0;
	TCCR1B = 0;
    TCCR1B |= (1 << WGM12); // set mode to: clear time compare (CTC)
    
    unsigned int compare_value = 124; 
    OCR1AL = compare_value & 0xff; // multiply frequency by 51191/65535=0.78125 -> 10000 Hz
    OCR1AH = (compare_value & 0xff00) >> 8; // multiply frequency by 51191/65535=0.78125 -> 10000 Hz
	TCNT1L = 0; // reset counter
    TCNT1H = 0; // reset counter
    
	TIMSK1 |= (1 << OCIE1A); // enable interrupt for timer1A
    TCCR1B |= (1 << CS11) | (1 << CS10); // Start the timer1, prescale 1/64 = 7812.5 Hz @ 8 MHz    
    
	sei();    
}

long clock_get_system_time_ms()
{
    return __system_time_ms;
}

void clock_regiter_task(void (*callback)(), long interval_ms )
{
    if (__callback_count == MAX_TASK_CALLBACKS) {
        error_report("Task callback table full.");
        return;
    }
    __callback[__callback_count] = callback;
    __callback_intervals_ms[__callback_count] = interval_ms;
    __callback_time_from_last_call_ms[__callback_count] = __system_time_ms;
    __callback_count++;
}

void execute_task(int task_id)
{
    if (task_id >= 0 && task_id < __callback_count) {
        (__callback[task_id])();    
    }        
}

ISR (TIMER1_COMPA_vect)
{
    __tics++; 
    if (__tics % (TICK_FREQUENCY / 1000) == 0) {
        __system_time_ms++;
        
        for (int i = 0; i < __callback_count; i++) {
            if (__system_time_ms - __callback_time_from_last_call_ms[i] > __callback_intervals_ms[i] ) {
                __callback_time_from_last_call_ms[i] = __system_time_ms;
                execute_task(i);
            }
        }
    }        
} 
