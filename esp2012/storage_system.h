/*
 * storage_system.h
 *
 * Created: 1/15/2013 19:48:36
 *  Author: mattiku
 */ 


#ifndef STORAGE_SYSTEM_H_
#define STORAGE_SYSTEM_H_

#define DEVICE_MODE_PLAY 1
#define DEVICE_MODE_CONTROL 2

void storage_system_init();

void storage_store_device_mode(char mode);
char storage_load_device_mode();

void storage_store_system_uptime_ms(unsigned long time_ms);
unsigned long storage_load_system_uptime_ms();

void storage_store_log_message(char* message);

// @param message is a buffer with SLOT_MESSAGE_BLOCK_SIZE size.
void storage_load_log_message(int slot, char* message, int max_size);

#endif /* STORAGE_SYSTEM_H_ */