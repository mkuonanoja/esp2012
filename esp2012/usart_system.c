/*
 * usart_system.c
 *
 * Created: 1/20/2013 16:14:40
 *  Author: mattiku
 */ 

#include "usart_system.h"
#include "error_system.h"
#include "led_system.h"
#include "clock_system.h"
#include <avr/io.h>
#include <avr/interrupt.h>
#include <string.h>

#define USART_BUFFER_SIZE_IN 200
#define USART_BUFFER_SIZE_OUT 600

#define MAX_LINE_SIZE 30

#define BLUETOOTH_STATUS_LED_TIMEOUT_MS 10

static unsigned char usart_buffer_in[USART_BUFFER_SIZE_IN];
static unsigned char usart_buffer_out[USART_BUFFER_SIZE_OUT];

static unsigned int __line_counter = 0;
static unsigned char __line_delimiter = 13;

static volatile unsigned int buffer_in_size = 0;
static volatile unsigned int buffer_in_read_index = 0;
static volatile unsigned int buffer_in_write_index = 0;

static volatile unsigned int buffer_out_size = 0;
static volatile unsigned int buffer_out_read_index = 0;
static volatile unsigned int buffer_out_write_index = 0;

void send_next_byte_from_buffer_out();	
void reset_bluetooth_status_led();

void usart_system_init(long baud_rate)
{
    cli();
    //UCSRA0A = 0x00;
    //UBBRL = ((MCU_CLOCK_SPEED_HZ)/(16x*USART_BAUD_RATE))-1;
    UCSR0B = (1 << TXEN0) | (1 << RXEN0); // enable transmitting and receiving
    UCSR0C = (1 << UCSZ01) | (1 << UCSZ00); // async, no parity, 1 stop bit, 8 data bits
    switch (baud_rate) {
        case 2400: UBRR0 = 207; break; //((MCU_CLOCK_SPEED_HZ)/(16*USART_BAUD_RATE))-1; // baud rate 2400 @ 8 MHz
        case 9600: UBRR0 = 51; break; //((MCU_CLOCK_SPEED_HZ)/(16*USART_BAUD_RATE))-1; // baud rate 9600 @ 8 MHz
        case 38400: UBRR0 = 12; break; //((MCU_CLOCK_SPEED_HZ)/(16*USART_BAUD_RATE))-1;  baud rate 38.4K @ 8 MHz        
        case 115200: UBRR0 = 3; break; //((MCU_CLOCK_SPEED_HZ)/(16*USART_BAUD_RATE))-1; baud rate 115.2K @ 8 MHz
    }    
    UCSR0B |= (1 << RXCIE0); // enable RX interrupt
    UCSR0B |= (1 << TXCIE0); // enable TX interrupt
    sei(); // enable interrupt
    clock_regiter_task(reset_bluetooth_status_led, BLUETOOTH_STATUS_LED_TIMEOUT_MS);
}

void usart_send_text(char* text)
{
    unsigned char sreg = SREG;
    if (buffer_out_size >= USART_BUFFER_SIZE_OUT) {
        error_report("Bluetooth buffer out overflow.");
        return;
    }
    int text_length = strlen(text);
    for(int i = 0; i < text_length; i++) {
        usart_buffer_out[buffer_out_write_index] = (unsigned char)(text[i]);
        buffer_out_write_index = (buffer_out_write_index + 1) % USART_BUFFER_SIZE_OUT;
        sreg = SREG;
        cli(); // disable interrupts
        buffer_out_size++;
        SREG = sreg; // restore interrupts
        if (buffer_out_size >= USART_BUFFER_SIZE_OUT) {
            error_report("Bluetooth buffer out overflow.");
            break;
        }
    }
    
    send_next_byte_from_buffer_out();	
}

int usart_get_text(char* text, int max_length)
{
    unsigned char data = 0;
    int i = 0;
    unsigned char sreg = SREG;
    cli();
    while(buffer_in_size > 0 && i < max_length) {
        data = usart_buffer_in[buffer_in_read_index];
        text[i++] = data;
        buffer_in_read_index = (buffer_in_read_index + 1 ) % USART_BUFFER_SIZE_IN;
        buffer_in_size--;
        if (data == __line_delimiter) {
            __line_counter--;
        }
    }
    SREG = sreg;
    return i;
}

void usart_set_line_delimiter(char delimiter)
{
    __line_delimiter = delimiter;
}

#define LINE_FEED_CHAR 10
#define CARRIAGE_RETURN_CHAR 13
    
int usart_get_next_line(char* text, int max_size)
{
    if (__line_counter == 0) {
        strncpy(text, "", max_size);
        return -1;
    }
    
    if (max_size > MAX_LINE_SIZE)
        max_size = MAX_LINE_SIZE;

    unsigned char data = 0;
    int i = 0;
    unsigned char sreg = SREG;
    cli(); // disable interrupts
    while(data != __line_delimiter && i < max_size-1) {
        data = usart_buffer_in[buffer_in_read_index];
        if (data != LINE_FEED_CHAR && data != __line_delimiter && data != CARRIAGE_RETURN_CHAR) {
            text[i] = data;
            i++;
        }
        buffer_in_read_index = (buffer_in_read_index + 1 ) % USART_BUFFER_SIZE_IN;
        buffer_in_size--;
        if (data == __line_delimiter) {
            __line_counter--;
            break;
        }
        if (buffer_in_size <= 0)
            break;
    }
    SREG = sreg; // restore interrupts
    text[i] = '\0';

    return i;	
}

void send_next_byte_from_buffer_out()
{
    //  	if (UCSR0A & (1 << RXC0))
    //  		return;
    unsigned char data = 0;
    unsigned char sreg;
    
    if (buffer_out_size > 0)  {
        data = usart_buffer_out[buffer_out_read_index];
        buffer_out_read_index = (buffer_out_read_index + 1) % USART_BUFFER_SIZE_OUT;
        sreg = SREG;
        cli(); // disable interrupts
        buffer_out_size--;
        SREG = sreg; // restore interrupts
        while ( !(UCSR0A & (1 << UDRE0)) ); // wait for UDRE flag
        UDR0 = data;
	}
}

void store_to_buffer_in(unsigned char data)
{
	if (buffer_in_size < USART_BUFFER_SIZE_IN) {
		if (buffer_in_size == USART_BUFFER_SIZE_IN - 1) {
			data = __line_delimiter; // simulate line delimiter character -> raises line count
		}
		usart_buffer_in[buffer_in_write_index] = data;
		buffer_in_write_index = (buffer_in_write_index + 1) % USART_BUFFER_SIZE_IN;
		buffer_in_size++;
		if (data == __line_delimiter) {
			__line_counter++;
		}
	} else {
 		error_report("Bluetooth buffer in overflow.");
	}
}

// Handles incoming data
ISR (USART0_RX_vect)
{
	led_set(LED_STATUS_BLUETOOTH, LED_STATE_ON);
 	while ( !(UCSR0A & (1 << RXC0)) ) {}; // wait for RXC flag
 	unsigned char data = UDR0;
	store_to_buffer_in(data);
}

// handles outgoing data
ISR (USART0_TX_vect)
{
	led_set(LED_STATUS_BLUETOOTH, LED_STATE_ON);
 	if (buffer_out_size > 0)
 		send_next_byte_from_buffer_out();
}

void reset_bluetooth_status_led()
{
    led_set(LED_STATUS_BLUETOOTH, LED_STATE_OFF);
}
