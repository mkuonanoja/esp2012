/*
 * Created: 1/12/2013 22:48:52
 *  Author: mattiku
 */ 

// For unit testing, uncomment one of the following lines to enable a unit testing mode
//
 //#define ACCELERATION_SENSOR_TEST_ENABLED
// #define PRESSURE_SENSOR_TEST_ENABLED
 //#define AUDIO_PLAYBACK_TEST_ENABLED
// #define LED_TEST_ENABLED
// define BUTTON_A_TEST_ENABLED
// #define MODE_BUTTON_TEST_ENABLED
//#define JTAG_TEST_ENABLED


#ifndef TEST_SYSTEM_H_
#define TEST_SYSTEM_H_

#if defined(ACCELERATION_SENSOR_TEST_ENABLED) || defined(PRESSURE_SENSOR_TEST_ENABLED) || defined(AUDIO_PLAYBACK_TEST_ENABLED) || defined(LED_TEST_ENABLED) || defined(BUTTON_A_TEST_ENABLED) || defined(MODE_BUTTON_TEST_ENABLED) || defined(JTAG_TEST_ENABLED)
	#define TEST_MODE_ACTIVE
#endif

void test_main_loop();

#endif /* TEST_SYSTEM_H_ */