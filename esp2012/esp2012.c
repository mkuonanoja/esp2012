/*
 * esp2012.c
 *
 * Created: 1/13/2013 19:35:00
 *  Author: mattiku
 */ 

#define F_CPU 8000000UL 

#include "clock_system.h"
#include "button_system.h"
#include "led_system.h"
#include "sensor_system.h"
#include "audio_system.h"
#include "error_system.h"
#include "test_system.h"
#include "storage_system.h"
#include "bluetooth_system.h"
#include "play_mode.h"
#include "control_mode.h"
#include <avr/io.h>
#include <util/delay.h>
#include <avr/interrupt.h>

#define DEVICE_UPTIME_STORE_DELAY_MS 60000

void main_loop();
void check_device_mode();
void set_RGB_led_initial_status();
void blink_device_mode_led();
void blink_power_led();
void update_device_uptime();

int main(void)
{
    clock_system_init(); // timer1
	buttom_system_init(); // int0
 	led_system_init(); 
 	led_set(LED_STATUS_POWER, LED_STATE_OFF);
 	led_set(LED_STATUS_BLUETOOTH, LED_STATE_OFF);
 	led_set(LED_STATUS_ERROR, LED_STATE_OFF);
    bluetooth_system_init(); // usart
    audio_system_init(); // timer0, timer2
	sensor_system_init(); // adc, twi
    error_system_init();
    
    clock_regiter_task(update_device_uptime, DEVICE_UPTIME_STORE_DELAY_MS);

#ifdef TEST_MODE_ACTIVE
	test_main_loop();
#else		
	main_loop();
#endif	
}

void main_loop()
{
    set_RGB_led_initial_status();
    bluetooth_send_text("\r\n>> ALL SYSTEMS INITIALIZED.\r\n");    
    
    while(1)
    {
	    _delay_ms(10); // max 100 Hz for the main loop
        blink_power_led();
       
		check_device_mode();
		
        if (storage_load_device_mode() == DEVICE_MODE_PLAY) {
        	recognice_gestures();
            update_led_effects();
	        blink_device_mode_led();
        } else {
            led_set(LED_DEVICE_MODE, LED_STATE_OFF);
        }            
        handle_commands();
    }
}

void check_device_mode()
{
    if (button_is_button_B_pressed()) {
        button_reset_button_B();
        int device_mode = storage_load_device_mode();
        if (device_mode == DEVICE_MODE_PLAY)
            device_mode = DEVICE_MODE_CONTROL;
        else
            device_mode = DEVICE_MODE_PLAY;
        storage_store_device_mode(device_mode);
    }
}

void set_RGB_led_initial_status()
{
    if (storage_load_device_mode() == DEVICE_MODE_CONTROL) {
        led_set_rgb(LED_RGB_LEFT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
        led_set_rgb(LED_RGB_RIGHT, LED_STATE_OFF, LED_STATE_OFF, LED_STATE_ON);
    }
}

void blink_device_mode_led()
{
    static int led_state = 0;
    led_state++;
    if (led_state % 100 == 0) {
        led_set(LED_DEVICE_MODE, LED_STATE_ON);
    } else {
        led_set(LED_DEVICE_MODE, LED_STATE_OFF);
    }
}    

void blink_power_led()
{
    static int led_state = 0;
    led_state++;
    if (led_state % 100 == 0) {
        led_set(LED_STATUS_POWER, LED_STATE_ON);
    } else {
        led_set(LED_STATUS_POWER, LED_STATE_OFF);
    }
}

void update_device_uptime()
{
    unsigned long uptime_ms = storage_load_system_uptime_ms();
    uptime_ms += DEVICE_UPTIME_STORE_DELAY_MS; 
    storage_store_system_uptime_ms(uptime_ms);
}
