/*
 * Created: 1/12/2013 22:53:08
 *  Author: mattiku
 */ 
#include "sensor_system.h"
#include "adc_system.h"
#include "acceleration_sensor_adxl345_system.h"
#include "clock_system.h"

#define DATA_UPDATE_DELAY_MS 100

void update_acceleration_data();

void sensor_system_init()
{
	adc_system_init();
    acceleration_sensor_adxl345_system_init(ACCELERATION_SENSOR_ADXL345_RANGE_2_G);
    clock_regiter_task(update_acceleration_data, DATA_UPDATE_DELAY_MS);
}

unsigned char sensor_get_pressure()
{
	return adc_get_value();	
}

void update_acceleration_data()
{
    acceleration_sensor_adxl345_refresh();
}

double sensor_get_acceleration_x()
{
    // todo convert to float
    return acceleration_sensor_adxl345_get_x();	    
}

double sensor_get_acceleration_y()
{
    // todo convert to float
    return acceleration_sensor_adxl345_get_y();	    
}

double sensor_get_acceleration_z()
{
    // todo convert to float
    return acceleration_sensor_adxl345_get_z();	        
}
