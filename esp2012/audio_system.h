/*
 * Created: 1/12/2013 22:48:52
 *  Author: mattiku
 */ 

#ifndef AUDIO_SYSTEM_H_
#define AUDIO_SYSTEM_H_

#define SOUND_1 1
#define SOUND_2 2
#define SOUND_3 3

void audio_system_init();

// @param sound: AUDIO_SAMPLE_1, AUDIO_SAMPLE_2 or AUDIO_SAMPLE_3
// If earlier audio sample is playing it will be stopped.
void audio_play_sound(unsigned char sound);
void audio_stop_sound();

// todo: MODE TO SEPARATE FILE
//long clock_get_system_time_ms();

#endif /* AUDIO_SYSTEM_H_ */