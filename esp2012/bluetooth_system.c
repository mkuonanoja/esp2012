/*
 * Created: 1/12/2013 22:53:08
 *  Author: mattiku
 */ 

#define F_CPU 8000000UL

#include "bluetooth_system.h"
#include "error_system.h"
#include "usart_system.h"
#include "bluetooth_module_linvor_1_5_system.h"

#define BLUETOOTH_DEVICE_NAME "ESP-2012-v.0.4" // Any name, max 32 characters
#define USART_BAUD_RATE 9600
#define COMMAND_SEPARATOR_CHARACTER 13 // carriage return

static char bluetooth_system_initialized = 0;

void bluetooth_system_init()
{
    usart_system_init(USART_BAUD_RATE);
    usart_set_line_delimiter(COMMAND_SEPARATOR_CHARACTER);
    bluetooth_module_linvor_1_5_system_init(USART_BAUD_RATE);
//    bluetooth_module_linvor_1_5_set_device_name(BLUETOOTH_DEVICE_NAME);
//    bluetooth_module_linvor_1_5_set_device_pin_code("1234");
	bluetooth_system_initialized = 1;
}

void bluetooth_send_text(char* text)
{
    if (!bluetooth_system_initialized) {
        error_report("Bluetooth system not initialized.");
        return;
    }
    
    usart_send_text(text);
}

int bluetooth_get_next_command(char* text, int max_length)
{
    return usart_get_next_line(text, max_length);
}

void bluetooth_get_device_version(char* version, int max_length)
{
    bluetooth_module_linvor_1_5_get_device_version(version, max_length);
}

void bluetooth_get_device_name(char* name, int max_length)
{
    bluetooth_module_linvor_1_5_get_device_name(name, max_length);
}

void bluetooth_reset_command_buffer()
{
    const int BUFFER_SIZE = 100;
    char command_buffer[BUFFER_SIZE];
    int command_size = 0;
    do {
        command_size = bluetooth_get_next_command(command_buffer, BUFFER_SIZE);
    } while (command_size > 0);
}
