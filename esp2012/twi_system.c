/*
 * twi_system.c
 *
 * Created: 1/16/2013 20:04:31
 *  Author: mattiku
 */ 

#include "twi_system.h"
#include <util/twi.h>

static int error_flag = NO_ERRORS;

#define WAITING_TWINT_FLAG_MAX_TRY 1000

void twi_system_init()
{
	// TWI bus frequency	
 	TWSR &= ~((1 << TWPS1) | (1 << TWPS0));   // Select Prescaler of 1 (TWPS1 = 0, TWPS0 = 0)
	TWBR = 0x08;	// 100 kHz @ 8 MHz   = 16 + 2 * TWBR * 4 ^ TWPS
    
    // enable TW interrupts
//    TWCR |= TWIE; 
    error_flag = NO_ERRORS;
}

#define TWCR_START ((1 << TWINT) | (1 << TWSTA) | (1 << TWEN))
#define TWCR_STOP ((1 << TWINT) | (1 << TWSTO) | (1 << TWEN))
#define TWCR_SEND ((1 << TWINT) | (1 << TWEN))
#define TWCR_R_ACK ((1 << TWINT) | (1 << TWEA) | (1 << TWEN)) //receive byte and return ack to slave
#define TWCR_R_NACK ((1 << TWINT) | (1 << TWEN)) //receive byte and return nack to slave

#define WAIT_TWINT_FLAG while (!(TWCR & (1 << TWINT)));

// @return 1 if success, otherwise 0
char wait_for_TWINT_flag(int max_try)
{
    for (int i = 0; i < max_try && !(TWCR & (1 << TWINT)); i++) {
        
    }
    if (!(TWCR & (1 << TWINT))) 
        return 0;
    else
        return 1;
}

void twi_uninit()
{
	TWCR = 0; // Set TWEN = 0
    error_flag = NO_ERRORS;    
}

int twi_get_error_status()
{
    return error_flag;
}

void twi_send_start()
{
    error_flag = NO_ERRORS;
	TWCR = TWCR_START;	
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_START_FAILURE;
    if (TW_STATUS != TW_START) {
        error_flag = ERROR_START_FAILURE;
    }        
}

void twi_send_stop()
{
	TWCR = TWCR_STOP;	
    while(TWCR & (1 << TWSTO)); 
}

void twi_send_rep_start()
{
	TWCR = TWCR_START;
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_START_FAILURE;
	if (TW_STATUS != TW_REP_START) {
    	error_flag = ERROR_START_FAILURE;
	}
}

void twi_send_sla_w(unsigned char sla_address_w)
{
    TWDR = sla_address_w;
    TWCR = TWCR_SEND;
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_SLA_W_FAILURE;
    if (TW_STATUS != TW_MT_SLA_ACK) {
        error_flag = ERROR_SLA_W_FAILURE;
    }
}

void twi_send_sla_r(unsigned char sla_address_r)
{
    TWDR = sla_address_r;
    TWCR = TWCR_SEND;
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_DATA_FAILURE;
    if (TW_STATUS != TW_MR_SLA_ACK) {
        error_flag = ERROR_DATA_FAILURE;
    }
}

void twi_send_data_8bit(unsigned char data)
{
    TWDR = data;
    TWCR = TWCR_SEND; 
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_DATA_FAILURE;
    if (TW_STATUS != TW_MT_DATA_ACK) {
        error_flag = ERROR_DATA_FAILURE;
    }
}

void twi_read_data_8bit_ack(unsigned char* data)
{
    TWCR = (1 << TWINT) | (1 << TWEN) | (1 << TWEA); // Start reading transmission with ACK
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_DATA_FAILURE;
    if (TW_STATUS == TW_MR_DATA_ACK) {
        *data = TWDR;
    }    
}

void twi_read_data_8bit_nack(unsigned char* data)
{
    TWCR = TWCR_SEND; // Start reading transmission without ACK
    if (!wait_for_TWINT_flag(WAITING_TWINT_FLAG_MAX_TRY))
        error_flag = ERROR_DATA_FAILURE;
    if (TW_STATUS == TW_MR_DATA_NACK) {
        *data = TWDR;
    }
}
