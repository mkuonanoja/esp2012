/*
 * Created: 1/12/2013 22:48:52
 *  Author: mattiku
 */ 


#ifndef LED_SYSTEM_H_
#define LED_SYSTEM_H_

#define LED_STATE_ON 1
#define LED_STATE_OFF 2

#define LED_STATUS_POWER 10
#define LED_STATUS_BLUETOOTH 11
#define LED_STATUS_ERROR 12
#define LED_DEVICE_MODE 13
#define LED_RGB_LEFT 20
#define LED_RGB_RIGHT 21

void led_system_init();

// @param led_id 
// @param state [LED_STATE_ON, LED_STATE_OFF]
void led_set(unsigned led_id, unsigned char state);

// @param led_id is [LED_RGB_LEFT, LED_RGB_RIGHT]
// @param red_state [LED_STATE_ON, LED_STATE_OFF]
// @param green_state [LED_STATE_ON, LED_STATE_OFF]
// @param blue_state [LED_STATE_ON, LED_STATE_OFF]
void led_set_rgb(unsigned char led_id, unsigned char red_state, unsigned char green_state, unsigned char blue_state);
void led_get_led_rgb(unsigned char led_id, unsigned char *red_state, unsigned char *green_state, unsigned char *blue_state);

#endif /* LED_SYSTEM_H_ */