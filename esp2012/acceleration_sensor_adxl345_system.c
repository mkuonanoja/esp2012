/*
 * acceleration_sensor_adxl345_system.c
 *
 * Created: 1/19/2013 14:03:30
 *  Author: mattiku
 */ 

#include "acceleration_sensor_adxl345_system.h"
#include "twi_system.h"
#include "error_system.h"
#define F_CPU 8000000UL 
#include <avr/io.h>
#include <util/delay.h>

#define ADXL345_DEVICE_ID 0xE5

#define SLA_R_ACCELERATION_SENSOR_MODULE 0xA7 // name #1 (+R)
#define SLA_W_ACCELERATION_SENSOR_MODULE 0xA6 // name #1 (+W)
//#define SLA_R_ACCELERATION_SENSOR_MODULE 0x3B // name #2 (+R)
//#define SLA_W_ACCELERATION_SENSOR_MODULE 0x3A // name #2 (+W)

#define MAX_MEASURE_VALUE_10_BIT 1024
#define MAX_MEASURE_VALUE_15_BIT 32768

#define REG_ADD_DEVID 0x00
#define REG_ADD_BW_RATE 0x2C
#define REG_ADD_POWER_CTR 0x2D
#define REG_ADD_INT_ENABLE 0x2E
#define REG_ADD_INT_MAP 0x2F
#define REG_ADD_INT_SOURCE 0x30
#define REG_ADD_DATA_FORMAT 0x31
#define REG_ADD_DATAX0 0x32
#define REG_ADD_DATAX1 0x33
#define REG_ADD_DATAY0 0x34
#define REG_ADD_DATAY1 0x35
#define REG_ADD_DATAZ0 0x36
#define REG_ADD_DATAZ1 0x37
#define REG_ADD_FIFO_CTL 0x38
#define REG_ADD_FIFO_STATUS 0x39

static int acceleration_x = 0;
static int acceleration_y = 0;
static int acceleration_z = 0;

static unsigned char __device_id = 0;
static int __acceleration_sensor_range = 0;

void acceleration_sensor_register_write_8bit(unsigned char register_address, unsigned char data_8bit);
void acceleration_sensor_register_read_8bit(unsigned char register_address, unsigned char* data_8bit);
void acceleration_sensor_register_read_16bit(unsigned char register_address, unsigned int *data_16bit);
char get_data_ready_flag();
unsigned char get_device_id();
void fetch_acceleration_data_xyz();
void twi_system_init();
void set_range();
void set_measure_rate();
//	enable_interrupts();
//  map_interrupts();
void wakeup_device();
void setup_fifo_buffer();
void enable_acceleration_measure();
void enable_chip();

void acceleration_sensor_adxl345_system_init(int accleleration_sensor_range)
{
    enable_chip();
    _delay_ms(100);
    __acceleration_sensor_range = accleleration_sensor_range;
    twi_uninit();
    twi_system_init();
    __device_id = get_device_id();
    if (__device_id != ADXL345_DEVICE_ID) {
        error_report("Acceleration sensor module not found.");
        return;
    }
    set_range();
    set_measure_rate();
//	enable_interrupts();
//  map_interrupts();
    wakeup_device();
    setup_fifo_buffer();
    enable_acceleration_measure();
}

double acceleration_sensor_adxl345_get_x()
{
    double acceleration_x_double = ((double)((long)acceleration_x * __acceleration_sensor_range)) / MAX_MEASURE_VALUE_15_BIT;
    return acceleration_x_double;
}

double acceleration_sensor_adxl345_get_y()
{
    double acceleration_y_double = ((double)((long)acceleration_y * __acceleration_sensor_range)) / MAX_MEASURE_VALUE_15_BIT;
    return acceleration_y_double;
}

double acceleration_sensor_adxl345_get_z()
{
    double acceleration_z_double = ((double)((long)acceleration_z * __acceleration_sensor_range)) / MAX_MEASURE_VALUE_15_BIT;
    return acceleration_z_double;
}

void acceleration_sensor_adxl345_refresh()
{
    fetch_acceleration_data_xyz();
}


void acceleration_sensor_register_write_8bit(unsigned char register_address, unsigned char data_8bit) {
    twi_send_start();
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: START");
        return;
    }
    
    twi_send_sla_w(SLA_W_ACCELERATION_SENSOR_MODULE);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SLA_W");        
        return;
    }

    twi_send_data_8bit(register_address);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SEND REGISTER ADDRESS");                
        return;
    }
    
    twi_send_data_8bit(data_8bit);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SEND DATA");        
        return;
    }
    
    twi_send_stop();
}

void acceleration_sensor_register_read_8bit(unsigned char register_address, unsigned char *data_8bit) {
    twi_send_start();
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: START");        
        return;
    }
    twi_send_sla_w(SLA_W_ACCELERATION_SENSOR_MODULE);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SLA_W");        
        return;
    }
    
    twi_send_data_8bit(register_address);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: REGISTER ADDRESS");        
        return;
    }
    
    twi_send_rep_start();
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: REP_START");        
        return;
    }
    
    twi_send_sla_r(SLA_R_ACCELERATION_SENSOR_MODULE);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SLA_R");        
        return;
    }
    
    twi_read_data_8bit_nack(data_8bit);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: READ");        
        return;
    }
    
    twi_send_stop();
}

void acceleration_sensor_register_read_16bit(unsigned char register_address, unsigned int *data_16bit)
{
    unsigned char data_lower_8bit = 0;
    unsigned char data_upper_8bit = 0;

    twi_send_start();
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: START");        
        return;
    }
    twi_send_sla_w(SLA_W_ACCELERATION_SENSOR_MODULE);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SLA_W");        
        return;
    }
    
    twi_send_data_8bit(register_address);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: REGISTER ADDRESS");        
        return;
    }
    
    twi_send_rep_start();
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: REP_START");        
        return;
    }
    
    twi_send_sla_r(SLA_R_ACCELERATION_SENSOR_MODULE);
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: SLA_R");        
        return;
    }
    
    twi_read_data_8bit_ack(&data_lower_8bit); // LSB
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: READ 1st BYTE");
        return;
    }
    
    twi_read_data_8bit_nack(&data_upper_8bit); // MSB
    if (twi_get_error_status() != NO_ERRORS) {
        error_report("TWI ERROR: READ 2nd BYTE");
        return;
    }

    twi_send_stop();

    // input Data1=[DDDD DDDD] Data0=[DD00 0000]
    *data_16bit = ((unsigned int)data_upper_8bit) << 8; // + data_lower_8bit;
    *data_16bit |= data_lower_8bit; // add lower 8 bits.
}

// @return 1 if DATA_READY flag is set, otherwise return 0
char get_data_ready_flag()
{
    unsigned char interrupt_sources = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_INT_SOURCE, &interrupt_sources);
    if (interrupt_sources && (1 << 7)) // DATA_READY flag
        return 1;
    else
        return 0;
}

unsigned char get_device_id()
{
    unsigned char devid_register = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_DEVID,  &devid_register);
    return devid_register;
}
        
void fetch_acceleration_data_xyz()
{
    if (get_data_ready_flag()) {
        int acceleration_x_data = 0;
        acceleration_sensor_register_read_16bit(REG_ADD_DATAX0, (unsigned int*)&acceleration_x_data);
        acceleration_x = acceleration_x_data;
        
        int acceleration_y_data = 0;
        acceleration_sensor_register_read_16bit(REG_ADD_DATAY0, (unsigned int*)&acceleration_y_data);
        acceleration_y = acceleration_y_data;
        
        int acceleration_z_data = 0;
        acceleration_sensor_register_read_16bit(REG_ADD_DATAZ0, (unsigned int*)&acceleration_z_data);
        acceleration_z = acceleration_z_data;
    }
}

void set_range()
{
    unsigned char data_format_register = 0;
    
    acceleration_sensor_register_read_8bit(REG_ADD_DATA_FORMAT,  &data_format_register);
    
    switch(__acceleration_sensor_range) {
        case ACCELERATION_SENSOR_ADXL345_RANGE_2_G:
            data_format_register &= ~(1 << 0); // D0 = 0 -> +/- 2G
            data_format_register &= ~(1 << 1); // D1 = 0 -> +/- 2G
        break;
        case ACCELERATION_SENSOR_ADXL345_RANGE_4_G:
            data_format_register |= (1 << 0);  // D0 = 1 -> +/- 4G
            data_format_register &= ~(1 << 1); // D1 = 0 -> +/- 4G
        break;
        case ACCELERATION_SENSOR_ADXL345_RANGE_8_G:
            data_format_register &= ~(1 << 0); // D0 = 0 -> +/- 8G
            data_format_register |= (1 << 1);  // D1 = 1 -> +/- 8G
        break;
        case ACCELERATION_SENSOR_ADXL345_RANGE_16_G:
            data_format_register |= (1 << 0);  // D0 = 1 -> +/- 16G
            data_format_register |= (1 << 1);  // D1 = 1 -> +/- 16G
        break;
    }
//    data_format_register &= ~(1 << 2); // Justify = 0 (Right-justified data, LSB is D0 of Data[X,Y,Z]0 register (10 bit resolution)
    data_format_register |= (1 << 2); // Justify = 1 (Left-justified data, LSB is D6 of Data[X,Y,Z]0 register (10 bit resolution)
    data_format_register &= ~(1 << 7); // SELF TEST = 0
    
    acceleration_sensor_register_write_8bit(REG_ADD_DATA_FORMAT, data_format_register);
}

void enable_interrupts()
{
    unsigned char enabled_interrupts = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_INT_ENABLE, &enabled_interrupts);
    enabled_interrupts = 0;
    enabled_interrupts |= (1 << 7); // DATA_READY
    acceleration_sensor_register_write_8bit(REG_ADD_INT_ENABLE, enabled_interrupts);
}

void map_interrupts()
{
    unsigned char map_interrupts = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_INT_MAP, &map_interrupts);
    map_interrupts &= ~(1 << 7); // DATA_READY -> INT1
    map_interrupts |= (1 << 7); // DATA_READY -> INT2
    acceleration_sensor_register_write_8bit(REG_ADD_INT_MAP, map_interrupts);
}

void enable_acceleration_measure()
{
    unsigned char power_crl_register = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_POWER_CTR, &power_crl_register);

    power_crl_register |= (1 << 3); //Measure = 1
    acceleration_sensor_register_write_8bit(REG_ADD_POWER_CTR, power_crl_register);
}

void set_measure_rate()
{
    unsigned char bw_rate_register = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_BW_RATE, &bw_rate_register);
    
    bw_rate_register = 0; // we define all the bits later
    bw_rate_register &= ~(1 << 4); // LOW_POWER = 0 (normal mode)
    bw_rate_register |= 0x0A; // rate = 100 Hz
    acceleration_sensor_register_write_8bit(REG_ADD_BW_RATE, bw_rate_register);
}

void wakeup_device()
{
    unsigned char power_ctr_register = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_POWER_CTR, &power_ctr_register);

    power_ctr_register &= ~(1 << 5); // Link = 0
    power_ctr_register &= ~(1 << 4); // AUTO_SLEEP = 0
    power_ctr_register &= ~(1 << 2); // Sleep = 0
    power_ctr_register |= 0x0A; // rate = 100 Hz
    acceleration_sensor_register_write_8bit(REG_ADD_POWER_CTR, power_ctr_register);
}

void setup_fifo_buffer()
{
    unsigned char fifo_ctl_register = 0;
    acceleration_sensor_register_read_8bit(REG_ADD_FIFO_CTL, &fifo_ctl_register);
    
    fifo_ctl_register = 0; // FIFO_MODE = 00 (Bypass)
                           // Trigger = 0 (trigger event -> INT1
                           // Samples = 00000 (not used in Bypass mode)
    acceleration_sensor_register_write_8bit(REG_ADD_POWER_CTR, fifo_ctl_register);
}

void enable_chip()
{
    DDRC |= (1 << PC6); // set pin 28 to output mode
    PORTC |= (1 << PC6); // set pin 28 to HIGH state
}